#include <stm32f0xx.h>
#include "parser.h"
#include "delay.h"
#include "convert.h"

#define 	ERROR		0
#define 	OK			1
#define 	BUSY		2

#define 	MAX_CIRCLE_BUF			1
#define 	MAX_BUF					980
#define 	LENGTH_URL				40
#define 	MAX_PARAM				6
#define 	LENGTH_PARAM_NAME		6
#define 	LENGTH_PARAM_VALUE		16
#define 	MAX_HEAD_SIZE			128

typedef struct{
	uint8_t ID[2];
	uint8_t METHOD[5];
	uint8_t URL[LENGTH_URL];
	uint8_t parameter_counter;
	uint8_t parameter_name[MAX_PARAM][LENGTH_PARAM_NAME];
	uint8_t parameter_value[MAX_PARAM][LENGTH_PARAM_VALUE];
} HTTP_Struct;

typedef struct {
	uint8_t parameter_name[MAX_PARAM][LENGTH_PARAM_NAME];
	uint8_t parameter_value[MAX_PARAM][LENGTH_PARAM_VALUE];
	uint8_t parameter_status[MAX_PARAM];
} Param_Struct;

HTTP_Struct cycleBuf[MAX_CIRCLE_BUF];
uint8_t tailBuf;
uint8_t headBuf;
uint8_t countBuf;

HTTP_Struct tmp;

uint16_t input_tailBuf;
uint16_t input_headBuf;
uint16_t input_countBuf;

uint8_t esp8266_send;

uint8_t esp8266_status, esp8266_pars, receive_ok, esp8266_error, esp8266_id_connected;
uint8_t esp8266_at_buf[MAX_HEAD_SIZE], esp8266_data_buf[MAX_BUF], esp8266_numberclient[2];

void ESP8266_Init();
void ESP8266_Window();
void USART_Write(uint8_t data);
void USART_Write_Text(uint8_t *text);
uint8_t ESP8266_CreateAP(char *SSID, char *password);
uint8_t ESP8266_ConnectToAP(char *SSID, char *password);
uint8_t ESP8266_CreateServer(char *port);
uint8_t ESP8266_ConnectToServer(uint8_t *IP, uint8_t *port, uint8_t *type);
uint8_t ESP8266_Write(uint8_t *text, uint8_t repply, uint16_t wait_answer_ms, uint16_t pause_after);
uint8_t ESP8266_Insert(uint8_t *buf, uint8_t *paste_buf, uint8_t pos);
uint8_t ESP8266_Get_Receive_Flag();
void ESP8266_Clear_Receive_Flag();
void ESP8266_Clear_Error_Flag();
uint8_t ESP8266_Get_Error_Flag();
uint8_t ESP8266_Send_HTTP_Head(uint8_t *ID, uint8_t *head, uint32_t content_length, uint8_t other_param);
uint8_t ESP8266_Send_Content(uint8_t *ID, uint8_t *content, uint16_t content_length);
uint8_t CircleBuf_Get(HTTP_Struct *str);
void CircleBuf_Place(HTTP_Struct event);
HTTP_Struct ESP8266_CopyMessage(HTTP_Struct str);
uint8_t ID_Is_Connected(uint8_t ID);
uint8_t InputBuf_Get(uint8_t *value);
void InputBuf_Place(uint8_t value);
