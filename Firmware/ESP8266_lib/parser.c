#include "parser.h"

#define TRUE   1
#define FALSE  0

uint8_t buf[SIZE_RECEIVE_BUF];
uint8_t *argv[AMOUNT_PAR];
uint8_t argc;

uint8_t i = 0;
uint8_t flag = 0;
uint16_t temp, size;

void PARS_Init(void)
{
  argc = 0;
  argv[0] = buf;
  flag = FALSE;
  i = 0;
}

uint8_t PARS_Length(uint8_t *text){
	uint8_t temp = 0;
	while (*text++){
		temp++;
	}
	return temp;
}

uint8_t PARS_GetSize(){
	return size;
}

void PARS_Parser(uint8_t sym)
{
   if (((sym != '\r') && (sym != '\n') && (sym != '+'))){
     if (i < SIZE_RECEIVE_BUF - 1){
        if (((sym != ' ') && (sym != ',') && (sym != ':') && (sym != '?') && (sym != '&') && (sym != '='))){
           if (!argc){
              argv[0] = buf;
              argc++;  
           }
           if (flag){
              if (argc < AMOUNT_PAR){
                 argv[argc] = &buf[i];   
                 argc++;
              }
              flag = FALSE; 
            }
            
            buf[i] = sym;
            i++;
        } else {
           if (!flag){
              buf[i] = 0;
              i++;
              flag = TRUE;
           }
            if (argc > 0){
              if (buf[i-2] == '>'){
                  buf[i] = 0;
                  argc = 0;
                  flag = FALSE;
                  i = 0;
              }
           }
        }
     }
     buf[i] = 0;
     return;
   }
   else{
      buf[i] = 0;
      if (argc){
         PARS_Handler(argc, argv);
      }
      else{
         //���� ����� ���-�� ��������
      }
      
      argc = 0;
      flag = FALSE;
      i = 0;
   }     
}

uint8_t PARS_EqualStr(uint8_t *s1, uint8_t *s2)
{
	uint8_t i = 0;
  
  while(s1[i] == s2[i] && s1[i] != '\0' && s2[i] != '\0'){
     i++;  
  }
  
  if (s1[i] =='\0' && s2[i] == '\0'){
     return TRUE;  
  }
  else{
     return FALSE;  
  }
}

uint8_t PARS_StrToUchar(uint8_t *s)
{
	uint8_t value = 0;
  
   while(*s == '0'){
     s++;
   }
   
   while(*s){ 
      value += (*s - 0x30);
      s++;
      if (*s){
         value *= 10;  
      }
   };
  
  return value;
}

uint16_t PARS_StrToUint(uint8_t *s)
{
	uint16_t value = 0;
  
   while(*s == '0'){
     s++;
   }
   
   while(*s){ 
      value += (*s - 0x30);
      s++;
      if (*s){
         value *= 10;  
      }
   };
  
  return value;
}
