#include "convert.h"

#define TRUE   1
#define FALSE  0

void BinToBCD(uint16_t bin, uint8_t *a, uint8_t *b, uint8_t *c, uint8_t *d, uint8_t *e){
	if (bin <= 65535){
		(*a) = (*b) = (*c) = (*d) = (*e) = 48;
		while (bin >= 10000){
			bin -= 10000;
			(*a)++;
		}
		while (bin >= 1000){
			bin -= 1000;
			(*b)++;
		}
		while (bin >= 100){
			bin -= 100;
			(*c)++;
		}
		while (bin >= 10){
			bin -= 10;
			(*d)++;
		}
		while (bin > 0){
			bin--;
			(*e)++;
		}
		if (((*a) > '9') || ((*b) > '9') || ((*c) > '9') || ((*d) > '9') || ((*e) > '9')){
			(*a) = (*b) = (*c) = (*d) = (*e) = 0;
		}
	}
}

void BinToBCD32(uint32_t bin, uint8_t *a, uint8_t *b, uint8_t *c, uint8_t *d, uint8_t *e, uint8_t *f, uint8_t *g, uint8_t *h){
	if (bin <= 99999999){
		(*a) = (*b) = (*c) = (*d) = (*e) = (*f) = (*g) = (*h) = 48;
		while (bin >= 10000000){
			bin -= 10000000;
			(*a)++;
		}
		while (bin >= 1000000){
			bin -= 1000000;
			(*b)++;
		}
		while (bin >= 100000){
			bin -= 100000;
			(*c)++;
		}
		while (bin >= 10000){
			bin -= 10000;
			(*d)++;
		}
		while (bin >= 1000){
			bin -= 1000;
			(*e)++;
		}
		while (bin >= 100){
			bin -= 100;
			(*f)++;
		}
		while (bin >= 10){
			bin -= 10;
			(*g)++;
		}
		while (bin > 0){
			bin--;
			(*h)++;
		}
		if (((*a) > '9') || ((*b) > '9') || ((*c) > '9') || ((*d) > '9') || ((*e) > '9') || ((*h) > '9')){
			(*a) = (*b) = (*c) = (*d) = (*e) = (*f) = (*g) = (*h) = 0;
		}
	}
}

void Insert(uint8_t *str1, uint8_t *str2, uint16_t pos, uint8_t *out){
	uint16_t size1, size2, i;
	size1 = Length(str1);
	size2 = Length(str2);
	i = size1 + size2;
	out[i] = 0;
	for (i = i-1; i > pos; i--){
		out[i] = str1[i-size2];
	}
	for (i = pos; i < pos + size2; i++){
		out[i] = str2[i-pos];
	}
	for (i = 0; i < pos; i++){
		out[i] = str1[i];
	}
}

void InsertStr(uint8_t *str1, uint8_t *str2, uint16_t pos){
	uint16_t size1, size2, i;
	size1 = Length(str1);
	size2 = Length(str2);
	i = size1 + size2;
	str1[i] = 0;
	for (i = i-1; i > pos; i--){
		str1[i] = str1[i-size2];
	}
	for (i = pos; i < pos + size2; i++){
		str1[i] = str2[i-pos];
	}
}

void Copy(uint8_t *buf, uint8_t *paste_buf, uint16_t size, uint16_t pos){
	uint16_t i;
	for (i = 0; i < size; i++){
		buf[i + pos] = paste_buf[i];
	}
	buf[i + pos] = 0;
}

uint32_t Length(uint8_t *text){
	uint32_t temp = 0;
	while (*text){
		text++;
		temp++;
	}
	return temp;
}

uint16_t Find(uint8_t *mas, uint8_t sym){
	uint16_t temp = 0;
	while(*mas){
		if (*mas != sym){
			mas++;
			temp++;
		} else return temp;
	}
	return 0xFFFF;
}

uint8_t IsStr(uint8_t *str, uint16_t len){
	while(*str){
		if (*str < ' ') return 1;
		str++;
	}
	return 0;
}

uint16_t FindStr(uint8_t *str1, uint8_t *str2, uint16_t *over){
	uint8_t *temp = str2;
	uint8_t i, stat;
	uint16_t pos = 0;
	stat = 0;
	i = 0;
	while (*str1){
		if ((*str1) != (*str2)){
			str2 = temp;
			stat = 0;
			i = 0;
		} else {
			i++;
			str2++;
			if (*str2 == 0){
				stat = 1;
				*over = pos+1;
				break;
			}
		}
		str1++;
		pos++;
	}
	if (!stat) pos = 0xFFFF;
	return (pos - i + 1);
}

uint8_t *Uint16ToStr(uint16_t val){
	uint8_t a, b, c, d, e, i = 0;
	BinToBCD(val, &a, &b, &c, &d, &e);
	if (a > 48) _num_buf[i++] = a;
	if ((a > 48) || (b > 48)) _num_buf[i++] = b;
	if ((a > 48) || (b > 48) || (c > 48)) _num_buf[i++] = c;
	if ((a > 48) || (b > 48) || (c > 48) || (d > 48)) _num_buf[i++] = d;
	_num_buf[i++] = e;
	_num_buf[i] = 0;
	return _num_buf;
}

uint8_t *Uint32ToStr(uint32_t val){
	uint8_t a, b, c, d, e, f, g, h, i = 0;
	BinToBCD32(val, &a, &b, &c, &d, &e, &f, &g, &h);
	if (a > 48) _num_buf[i++] = a;
	if ((a > 48) || (b > 48)) _num_buf[i++] = b;
	if ((a > 48) || (b > 48) || (c > 48)) _num_buf[i++] = c;
	if ((a > 48) || (b > 48) || (c > 48) || (d > 48)) _num_buf[i++] = d;
	if ((a > 48) || (b > 48) || (c > 48) || (d > 48) || (e > 48)) _num_buf[i++] = e;
	if ((a > 48) || (b > 48) || (c > 48) || (d > 48) || (e > 48) || (f > 48)) _num_buf[i++] = f;
	if ((a > 48) || (b > 48) || (c > 48) || (d > 48) || (e > 48) || (f > 48) || (g > 48)) _num_buf[i++] = g;
	_num_buf[i++] = h;
	_num_buf[i] = 0;
	return _num_buf;
}

uint8_t EqualStr(uint8_t *s1, uint8_t *s2){
  uint8_t i = 0;

  while(s1[i] == s2[i] && s1[i] != '\0' && s2[i] != '\0'){
     i++;
  }

  if (s1[i] =='\0' && s2[i] == '\0'){
     return TRUE;
  }
  else{
     return FALSE;
  }
}

uint8_t StrToUchar(uint8_t *s){
   uint8_t value = 0;

   while(*s == '0'){
     s++;
   }

   while(*s){
      value += (*s - 0x30);
      s++;
      if (*s){
         value *= 10;
      }
   };

  return value;
}

uint16_t StrToUint(uint8_t *s){
   uint16_t value = 0;

   while(*s == '0'){
     s++;
   }

   while(*s){
      value += (*s - 0x30);
      s++;
      if (*s){
         value *= 10;
      }
   };
  return value;
}
