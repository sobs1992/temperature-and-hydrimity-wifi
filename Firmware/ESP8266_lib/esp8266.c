#include "esp8266.h"

void PARS_Handler(uint8_t argc, uint8_t *argv[]){
	uint8_t i, j;
	if (argc == 1){
		if (PARS_EqualStr(argv[0], "ready")){
			esp8266_status = OK;
		} else
		if (PARS_EqualStr(argv[0], "OK")){
			esp8266_status = OK;
		} else
		if (PARS_EqualStr(argv[0], "ERROR")){
			esp8266_status = BUSY;
			esp8266_error = 1;
		} else
		if (PARS_EqualStr(argv[0], "FAIL")){
			esp8266_status = BUSY;
		}
	} else
	if (argc == 2){
		if (PARS_EqualStr(argv[1], "CONNECT")){
			i = StrToUchar(argv[0]);
			esp8266_id_connected |= (1 << i);
		} else
		if (PARS_EqualStr(argv[1], "CLOSED")){
			i = StrToUchar(argv[0]);
			esp8266_id_connected &= ~(1 << i);
		} else
		if (PARS_EqualStr(argv[0], "busy")){
			esp8266_status = BUSY;
			esp8266_error = 1;
		} else
		if (PARS_EqualStr(argv[0], "SEND")){
			esp8266_status = OK;
		} else
		if (PARS_EqualStr(argv[1], "change")){
			esp8266_status = OK;
		}
	}
	if (argc == 3){
		if (PARS_EqualStr(argv[0], "link")){
			esp8266_error = 1;
		}
	}
//	}else {
		if (PARS_EqualStr(argv[0], "IPD")){
			i = StrToUchar(argv[0]);
			esp8266_id_connected |= (1 << i);
			Copy(tmp.ID, argv[1], Length(argv[1]), 0);
			Copy(tmp.METHOD, argv[3], Length(argv[3]), 0);
			Copy(tmp.URL, argv[4], Length(argv[4]), 0);

			j = 5;
			tmp.parameter_counter = 0;
			while (!PARS_EqualStr(argv[j], "HTTP/1.1")){
				Copy(tmp.parameter_name[tmp.parameter_counter], argv[j], Length(argv[j]), 0);
				Copy(tmp.parameter_value[tmp.parameter_counter], argv[j + 1], Length(argv[j + 1]), 0);
				j+=2;
				tmp.parameter_counter++;
			}
			CircleBuf_Place(tmp);
		}
//	}
}

uint8_t ID_Is_Connected(uint8_t ID){
	return esp8266_id_connected & (1 << ID);
}

HTTP_Struct ESP8266_CopyMessage(HTTP_Struct str){
	HTTP_Struct temp;
	uint8_t i;
	Copy(temp.ID, str.ID, Length(str.ID), 0);
	Copy(temp.METHOD, str.METHOD, Length(str.METHOD), 0);
	Copy(temp.URL, str.URL, Length(str.URL), 0);
	temp.parameter_counter = str.parameter_counter;
	for (i = 0; i < temp.parameter_counter; i++){
		Copy(temp.parameter_name[i], str.parameter_name[i], Length(str.parameter_name[i]), 0);
		Copy(temp.parameter_value[i], str.parameter_value[i], Length(str.parameter_value[i]), 0);
	}
	return temp;
}

uint8_t ESP8266_Send_HTTP_Head(uint8_t *ID, uint8_t *head, uint32_t content_length, uint8_t other_param){
	uint8_t *size_content;
	uint8_t temp;
	uint32_t size_message, pause_counter, i;
	size_content = Uint32ToStr(content_length);
	size_message = Length(head) + Length(size_content) + 4;
	size_content = Uint32ToStr(size_message);

	temp = ESP8266_Insert(esp8266_at_buf, "AT+CIPSEND=", 0);
	temp = ESP8266_Insert(esp8266_at_buf, ID, temp);
	temp = ESP8266_Insert(esp8266_at_buf, ",", temp);
	temp = ESP8266_Insert(esp8266_at_buf, size_content, temp);
	esp8266_at_buf[temp] = 0;
	if (ESP8266_Write(esp8266_at_buf, 1, 1000, 0) == OK){
		temp = ESP8266_Insert(esp8266_at_buf, head, 0);
		temp = ESP8266_Insert(esp8266_at_buf, Uint32ToStr(content_length + other_param), temp);
		temp = ESP8266_Insert(esp8266_at_buf, "\r\n\r\n", temp);
		esp8266_status = 0;
		pause_counter = 0;
		for (i = 0; i < size_message; i++){
			USART_Write(esp8266_at_buf[i]);
			if (ESP8266_Get_Error_Flag()){
				ESP8266_Clear_Error_Flag();
				return ERROR;
			}
		}
		while(!esp8266_status){
			if (pause_counter < 5000){
				delay_ms(1);
				pause_counter++;
			} else break;
		}
		if (esp8266_status) return OK;
	}
	return ERROR;
}

uint8_t ESP8266_Send_Content(uint8_t *ID, uint8_t *content, uint16_t content_length){
	uint8_t *size, id_num;
	uint16_t temp, i, pause_counter;
	size = Uint16ToStr(content_length);
	temp = ESP8266_Insert(esp8266_at_buf, "AT+CIPSEND=", 0);
	temp = ESP8266_Insert(esp8266_at_buf, ID, temp);
	temp = ESP8266_Insert(esp8266_at_buf, ",", temp);
	temp = ESP8266_Insert(esp8266_at_buf, size, temp);
	esp8266_at_buf[temp] = 0;
	id_num = StrToUchar(ID);
	if (ESP8266_Write(esp8266_at_buf, 1, 1000, 0) == OK){
		esp8266_status = 0;
		pause_counter = 0;
		for (i = 0; i < content_length; i++){
			USART_Write(content[i]);
		}

		if (!ID_Is_Connected(id_num)){
			return ERROR;
		}

		while(!esp8266_status){
			if (pause_counter < 5000){
				delay_ms(1);
				pause_counter++;
			} else break;
			if (esp8266_status){
				return OK;
			}
		}
	}

	return ERROR;
}

void USART1_IRQHandler(){
	uint8_t temp;
	if (USART1 -> ISR & USART_ISR_RXNE){
		temp = USART1 -> RDR;
		TIM16 -> CNT = 1;
		if (!(TIM16 -> CR1 & TIM_CR1_CEN)) TIM16 -> CR1 |= TIM_CR1_CEN;
		InputBuf_Place(temp);
		if (temp == '>'){
			esp8266_send = 1;
		}
	} else {
	//	USART1 -> ICR = 0xFFFFFFFF;
	}
}

void TIM16_IRQHandler(){
	uint8_t temp;
	if (TIM16 -> SR & TIM_SR_UIF){
		TIM16 -> SR &= ~TIM_SR_UIF;
		TIM16 -> CR1 &= ~TIM_CR1_CEN;
		TIM16 -> CNT = 0;
		while (InputBuf_Get(&temp)){
			PARS_Parser(temp);
		}
	}
}

void USART_Write(uint8_t data){
	USART1 -> TDR = data;
	while(!(USART1 -> ISR & USART_ISR_TC));
}

void USART_Write_Text(uint8_t *text){
	while(*text){
		USART_Write(*text++);
	}
}

uint8_t ESP8266_CreateAP(char *SSID, char *password){
	uint8_t temp;
//	ESP8266_Write("AT+IPR=921600", 2, 4000, 4000);
	ESP8266_Write("AT+CWMODE=2", 2, 1000, 1000);
	ESP8266_Write("AT+RST", 2, 5000, 5000);
	ESP8266_Write("ATE0", 2, 1000, 1000);
	ESP8266_Write("AT+CIPMUX=1", 2, 1000, 1000);
	temp = ESP8266_Insert(esp8266_at_buf, "AT+CWSAP=", 0);
	temp = ESP8266_Insert(esp8266_at_buf, "\"", temp);
	temp = ESP8266_Insert(esp8266_at_buf, SSID, temp);
	temp = ESP8266_Insert(esp8266_at_buf, "\",\"", temp);
	temp = ESP8266_Insert(esp8266_at_buf, password, temp);
	temp = ESP8266_Insert(esp8266_at_buf, "\",1,3\0", temp);
	temp = ERROR;
	temp = ESP8266_Write(esp8266_at_buf, 2, 1000, 1000);
	return temp;
}

uint8_t ESP8266_ConnectToAP(char *SSID, char *password){
	uint8_t temp;
	ESP8266_Write("AT+CWMODE=1", 2, 1000, 1000);
	ESP8266_Write("AT+RST", 2, 5000, 5000);
	ESP8266_Write("ATE0", 2, 1000, 1000);
	temp = ESP8266_Insert(esp8266_at_buf, "AT+CWJAP=", 0);
	temp = ESP8266_Insert(esp8266_at_buf, "\"", temp);
	temp = ESP8266_Insert(esp8266_at_buf, SSID, temp);
	temp = ESP8266_Insert(esp8266_at_buf, "\",\"", temp);
	temp = ESP8266_Insert(esp8266_at_buf, password, temp);
	temp = ESP8266_Insert(esp8266_at_buf, "\"", temp);
	esp8266_at_buf[temp] = 0;
	temp = ESP8266_Write(esp8266_at_buf, 2, 30000, 5000);
	ESP8266_Write("AT+CIPMUX=1", 2, 2000, 1000);
	return temp;
}

uint8_t ESP8266_CreateServer(char *port){
	uint8_t temp;
	ESP8266_Write("AT+CIFSR", 2, 1000, 1000);
	temp = ESP8266_Insert(esp8266_at_buf, "AT+CIPSERVER=1,", 0);
	temp = ESP8266_Insert(esp8266_at_buf, port, temp);
	esp8266_at_buf[temp] = 0;
	do{
		temp = ESP8266_Write(esp8266_at_buf, 2, 10000, 1000);
	} while (temp != OK);
	return OK;
}

uint8_t ESP8266_ConnectToServer(uint8_t *IP, uint8_t *port, uint8_t *type){
	uint8_t temp;
	temp = ESP8266_Insert(esp8266_at_buf, "AT+CIPSTART=0,", 0);
	temp = ESP8266_Insert(esp8266_at_buf, "\"", temp);
	temp = ESP8266_Insert(esp8266_at_buf, type, temp);
	temp = ESP8266_Insert(esp8266_at_buf, "\",\"", temp);
	temp = ESP8266_Insert(esp8266_at_buf, IP, temp);
	temp = ESP8266_Insert(esp8266_at_buf, "\",", temp);
	temp = ESP8266_Insert(esp8266_at_buf, port, temp);
	esp8266_at_buf[temp] = 0;
	do{
		temp = ESP8266_Write(esp8266_at_buf, 2, 60000, 5000);
	} while (temp != OK);
	return OK;
}

uint8_t ESP8266_Write(uint8_t *text, uint8_t repply, uint16_t wait_answer_ms, uint16_t pause_after){
	uint8_t repply_counter, *buf, status;
	uint16_t pause_counter;

	buf = text;
	repply_counter = 0;
	status = ERROR;

	do{
		text = buf;
		esp8266_status = 0;
		pause_counter = 0;
		esp8266_error = 0;
		esp8266_send = 0;

		while(*text){
			USART_Write(*text++);
		}

		USART_Write('\r');
		USART_Write('\n');
		TIM16 -> CNT = 0;
		TIM16 -> CR1 |= TIM_CR1_CEN;
		while((!esp8266_send) && (!esp8266_status)){
			if (pause_counter < wait_answer_ms){
				delay_ms(1);
				pause_counter++;
			} else break;
		}
		if ((esp8266_send) || (esp8266_status)){
			repply_counter = repply;
			if ((esp8266_send) || (esp8266_status == OK)) status = OK;
		} else {
			if (repply_counter < repply) repply_counter++;
			else break;
		}
	} while (repply_counter < repply);
	delay_ms(pause_after);
	return status;
}

uint8_t ESP8266_Insert(uint8_t *buf, uint8_t *paste_buf, uint8_t pos){
	while(*paste_buf){
		buf[pos++] = *paste_buf++;
		if (pos == 0xFF){
			buf[255] = 0;
			break;
		}
	}
	return pos;
}

uint8_t ESP8266_Get_Error_Flag(){
	return esp8266_error;
}

void ESP8266_Clear_Error_Flag(){
	esp8266_error = 0;
}

void ESP8266_Init(){
	RCC -> AHBENR |= RCC_AHBENR_GPIOAEN;
	RCC -> APB2ENR |= RCC_APB2ENR_TIM16EN | RCC_APB2ENR_USART1EN;
	PARS_Init();

	esp8266_error = 0;
	esp8266_id_connected = 0;

	GPIOA -> MODER |= GPIO_MODER_MODER2_1 | GPIO_MODER_MODER3_1;
	GPIOA -> AFR[0] |= (1 << 8) | (1 << 12);

	USART1 -> BRR = 0x34;	//921600
	USART1 -> CR2 = 0;
	USART1 -> CR3 = USART_CR3_OVRDIS;
	USART1 -> CR1 = USART_CR1_RXNEIE | USART_CR1_TE | USART_CR1_RE | USART_CR1_UE;
	NVIC_SetPriority(USART1_IRQn, 2);
	NVIC_EnableIRQ(USART1_IRQn);

	TIM16 -> PSC = 52;
	TIM16 -> ARR = 100;
	TIM16 -> DIER |= TIM_DIER_UIE;
	NVIC_SetPriority(TIM16_IRQn, 1);
	NVIC_EnableIRQ(TIM16_IRQn);

	ESP8266_Write("ATE0", 2, 1000, 1000);
}

uint8_t CircleBuf_Get(HTTP_Struct *str){
	if (countBuf > 0){
		*str = ESP8266_CopyMessage(cycleBuf[headBuf]);
		countBuf--;
		headBuf++;
		if (headBuf >= MAX_CIRCLE_BUF) headBuf = 0;
		return 1;
	}
	return 0;
}

void CircleBuf_Place(HTTP_Struct event){
	if (countBuf < MAX_CIRCLE_BUF){
		cycleBuf[tailBuf] = ESP8266_CopyMessage(event);
		tailBuf++;
		if (tailBuf >= MAX_CIRCLE_BUF) tailBuf = 0;
		countBuf++;
	}
}

uint8_t InputBuf_Get(uint8_t *value){
	if (input_countBuf > 0){
		*value = esp8266_data_buf[input_headBuf];
		input_countBuf--;
		input_headBuf++;
		if (input_headBuf >= MAX_BUF) input_headBuf = 0;
		return 1;
	}
	return 0;
}

void InputBuf_Place(uint8_t value){
	if (input_countBuf < MAX_BUF){
		esp8266_data_buf[input_tailBuf] = value;
		input_tailBuf++;
		if (input_tailBuf >= MAX_BUF) input_tailBuf = 0;
		input_countBuf++;
	}
}
