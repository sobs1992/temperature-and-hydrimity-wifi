#include <stm32f0xx.h>

volatile static uint8_t _num_buf[10];

uint16_t FindStr(uint8_t *str1, uint8_t *str2, uint16_t *over);
void Insert(uint8_t *str1, uint8_t *str2, uint16_t pos, uint8_t *out);
void InsertStr(uint8_t *str1, uint8_t *str2, uint16_t pos);
void Copy(uint8_t *buf, uint8_t *paste_buf, uint16_t size, uint16_t pos);
uint32_t Length(uint8_t *text);
uint16_t Find(uint8_t *mas, uint8_t sym);
void BinToBCD(uint16_t bin, uint8_t *a, uint8_t *b, uint8_t *c, uint8_t *d, uint8_t *e);
void BinToBCD32(uint32_t bin, uint8_t *a, uint8_t *b, uint8_t *c, uint8_t *d, uint8_t *e, uint8_t *f, uint8_t *g, uint8_t *h);
uint8_t *Uint32ToStr(uint32_t val);
uint16_t StrToUint(uint8_t *s);
uint8_t StrToUchar(uint8_t *s);
uint8_t EqualStr(uint8_t *s1, uint8_t *s2);
uint8_t *Uint16ToStr(uint16_t val);
uint8_t IsStr(uint8_t *str, uint16_t len);
