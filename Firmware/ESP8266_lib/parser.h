#include <stdint.h>

/*����������� ��������� ������*/
#define SIZE_RECEIVE_BUF  900

/*���������� �������*/
#define AMOUNT_PAR 25

/*������� �������*/
void PARS_Init(void);
void PARS_Parser(uint8_t sym);

/*���������� ���������� ������. �� ������������ � ������ �����*/
extern void PARS_Handler(uint8_t argc, uint8_t *argv[]);

/*�������������� ������� ��� ������ �� ��������*/
uint8_t PARS_EqualStr(uint8_t *s1, uint8_t *s2);
uint8_t PARS_StrToUchar(uint8_t *s);
uint16_t PARS_StrToUint(uint8_t *s);
uint8_t PARS_Length(uint8_t *text);
uint8_t PARS_GetSize();

//#include "parser.c"
