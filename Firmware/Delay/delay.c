#include "delay.h"

void SysTick_Handler(){
	if (delay_cnt > 0) delay_cnt--;
}

void delay_ms(uint32_t ms){
	SysTick_Config(SystemCoreClock / 1000);
	NVIC_SetPriority(SysTick_IRQn, 0);

	delay_cnt = ms;
	while(delay_cnt);
}

void delay_us(uint32_t us){
	uint32_t i = us*3;
	while(i) i--;
}

