#include <stm32f0xx.h>
#include <stm32f0xx_conf.h>

volatile static uint32_t delay_cnt;

void delay_ms(uint32_t ms);
void delay_us(uint32_t us);
