#include <stm32f0xx.h>
#include "delay.h"

#define	OW_IN		((GPIOA -> IDR & GPIO_IDR_10) ? 1 : 0)
#define	OW_LOW		GPIOA -> BSRR = GPIO_BSRR_BR_10
#define	OW_HiZ		GPIOA -> BSRR = GPIO_BSRR_BS_10

void DHT11_Init();
void OW_Pin_In();
void OW_Pin_Out();
uint8_t DHT11_Byte();
uint8_t DHT11_Read(uint8_t *Rh, uint8_t *temp);
