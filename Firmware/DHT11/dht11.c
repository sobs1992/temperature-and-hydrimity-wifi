#include "dht11.h"

void DHT11_Init(){
	GPIOA -> OTYPER |= GPIO_OTYPER_OT_10;
	OW_Pin_In();
}

void OW_Pin_In(){
	GPIOA -> MODER &= ~(GPIO_MODER_MODER10);
}

void OW_Pin_Out(){
	GPIOA -> MODER |= GPIO_MODER_MODER10_0;
}

uint8_t DHT11_Byte(){
	uint16_t i = 0, t_out = 0;
	__disable_irq ();
	while (!OW_IN){
		if (t_out < 10000) t_out++; else break;
	}
	t_out = 0;
	while (OW_IN){
		i++;
		if (t_out < 10000) t_out++; else break;
	}
	__enable_irq ();
	if (i > 70) return 1;
	return 0;
}

uint8_t DHT11_Read(uint8_t *Rh, uint8_t *temp){
	uint8_t i, j;
	uint8_t data[5] = {0, 0, 0, 0, 0};
	uint16_t t_out = 0;

	OW_Pin_Out();
	OW_LOW;
	delay_ms(20);
	OW_HiZ;
	OW_Pin_In();
	delay_us(70);
	if (!OW_IN){
		while (!OW_IN){
			if (t_out < 10000) t_out++; else break;
		}
		t_out = 0;
		while (OW_IN){
			if (t_out < 10000) t_out++; else break;
		}

		for (i = 0; i < 5; i++){
			for (j = 8; j > 0; j--){
				data[i] |= (DHT11_Byte() << (j-1));
			}
		}
		j = data[0]+data[2];

		if (j == data[4]){
			*Rh = data[0];
			*temp = data[2];
			return 0;
		}
	}
	return 1;
}
