#include <stm32f0xx.h>
#include "delay.h"
#include "dht11.h"
#include "esp8266.h"
#include "at45db161d.h"

#define PWR_ON			GPIOA -> BSRR = GPIO_BSRR_BS_1
#define PWR_OFF			GPIOA -> BSRR = GPIO_BSRR_BR_1
#define WIFI_RESTART	GPIOA -> BSRR = GPIO_BSRR_BS_4
#define WIFI_START		GPIOA -> BSRR = GPIO_BSRR_BR_4
#define BUTTON			((GPIOA -> IDR & GPIO_IDR_0) ? 1 : 0)

uint8_t T, Rh, stat;
uint8_t num[6];
HTTP_Struct http;
uint16_t pos;

uint8_t hotspot;
uint8_t mssid[LENGTH_PARAM_VALUE], mpassw[LENGTH_PARAM_VALUE];
uint8_t sssid[LENGTH_PARAM_VALUE], spassw[LENGTH_PARAM_VALUE];
uint8_t ip[16];
uint8_t temp_buf[1024];

const uint8_t HTTP_MAIN[] = "HTTP/1.1 200 OK\r\nContent-Type: text/html; charset=windows-1251\r\nCache-Control: no-cache\r\nContent-Length: ";
const uint8_t HTTP_404[] = "HTTP/1.1 404 Not Found\r\nContent-Type: text/html; charset=windows-1251\r\nContent-Length: ";
const uint8_t Main_Page[] = "<html>\
<head>\
<meta http-equiv=\"REFRESH\" content=\"5; URL=/\">\
<style type=\"text/css\">\
h1 {\
font-size: 48pt;\
font-family: sans-serif;\
font-style: italic;\
color: #0000FF;\
}\
</style>\
<style>\
.bord {\
border: 4px double black;\
background: #fc3;\
padding: 10px;\
margin-bottom: 10px;\
text-align: center;\
}\
</style>\
<title>\
�����-����� ��������������\
</title>\
</head>\
<body bgcolor=\"lime\">\
<div style=\"border-radius: 50px;\" class=\"bord\"><H1>��������� = %</H1></div>\
<div style=\"border-radius: 50px;\" class=\"bord\"><H1>����������� = ��</H1></div>\
<p align='right'><a href=\"settings.html\">���������</a></p>\
</body>\
</html>";

const uint8_t Settings_Page[] = "<html>\
<head>\
<style>\
.bord {\
border: 4px double black;\
background: #fc3;\
margin-bottom: 10px;\
text-align: center;\
}\
</style>\
<title>\
���������\
</title>\
</head>\
<body bgcolor=\"lime\">\
<div style=\"border-radius: 50px;\" class=\"bord\">\		
<form method='GET'>\
<br><b><input type='radio' name=\"hots\" value=\"0\" >������� ����� �������<br>\
SSID: <input type='text' name='mssid' size='20' value=''><br>\
PASS: <input type='text' name='mpasw' size='20' value=''><br>\
IP: 192.168.4.1<br><br>\
<input type='radio' name=\"hots\" value=\"1\" >������������ � ����� �������<br>\
SSID: <input type='text' name='sssid' size='20' value=''><br>\
PASS: <input type='text' name='spasw' size='20' value=''><br>\
�������������� IP-�����:<br> <input type='text' name='ip' size='15' value=''><br><br>\
<input type='submit' value='���������'><br></b>\
</form>\
</div>\
<p align='left'><a href=\"/\">�� �������</a></p>\
</body>\
</html>";

const uint8_t Page_not_found[] = "<html>\
<head>\
<title>\
404 Page Not Found\
</title>\
</head>\
<body style=>\
<p align='center'>C������� �� �������</p>\
</body>\
</html>";

void EXTI0_1_IRQHandler(){
	delay_ms(500);
	if (BUTTON){
		PWR_OFF;
		while(1);
	}
	EXTI -> PR |= EXTI_PR_PR0;
}

void Flash_WriteSettings(){
	temp_buf[0] = hotspot;
	Copy(temp_buf, mssid, LENGTH_PARAM_VALUE, 1);
	Copy(temp_buf, mpassw, LENGTH_PARAM_VALUE, 1 + LENGTH_PARAM_VALUE);
	Copy(temp_buf, sssid, LENGTH_PARAM_VALUE, 1 + LENGTH_PARAM_VALUE * 2);
	Copy(temp_buf, spassw, LENGTH_PARAM_VALUE, 1 + LENGTH_PARAM_VALUE * 3);
	Copy(temp_buf, ip, 16, 1 + (LENGTH_PARAM_VALUE * 4));
	AT45DB161_PageProgram(0, temp_buf, LENGTH_PARAM_VALUE * 4 + 1 + 16);
}

void Flash_ReadSettings(){
	uint8_t temp = 0, i, point;
	AT45DB161_Read_Data(0, 0, 1, &hotspot);
	if ((hotspot != '0') && (hotspot != '1')){
		hotspot = '0';
		temp |= 0x01;
	}
	AT45DB161_Read_Data(0, 1, LENGTH_PARAM_VALUE, mssid);
	if (IsStr(mssid, Length(mssid))){
		Copy(mssid, "SOBS1234", 8, 0);
		temp |= 0x02;
	}
	AT45DB161_Read_Data(0, 1 + LENGTH_PARAM_VALUE, LENGTH_PARAM_VALUE, mpassw);
	if (IsStr(mpassw, Length(mpassw))){
		Copy(mpassw, "12345678", 8, 0);
		temp |= 0x04;
	}
	AT45DB161_Read_Data(0, 1 + LENGTH_PARAM_VALUE * 2, LENGTH_PARAM_VALUE, sssid);
	if (IsStr(sssid, Length(sssid))){
		Copy(sssid, "ZTE", 8, 0);
		temp |= 0x08;
	}
	AT45DB161_Read_Data(0, 1 + LENGTH_PARAM_VALUE * 3, LENGTH_PARAM_VALUE, spassw);
	if (IsStr(spassw, Length(spassw))){
		Copy(spassw, "12345678", 8, 0);
		temp |= 0x10;
	}
	point = 0;
	AT45DB161_Read_Data(0, 1 + LENGTH_PARAM_VALUE * 4, 16, ip);
	for (i = 0; i < 15; i++){
		if (ip[i] == '.') point++;
	}
	if (point != 3){
		Copy(ip, "192.168.1.100", 13, 0);
		temp |= 0x20;
	}
	if (temp){
		Flash_WriteSettings();
	}
}

void Default(){
	hotspot = '0';
	Copy(mssid, "SOBS1234", 8, 0);
	Copy(mpassw, "12345678", 8, 0);
	Copy(sssid, "ZTE", 8, 0);
	Copy(spassw, "12345678", 8, 0);
	Copy(ip, "192.168.1.100", 13, 0);
	Flash_WriteSettings();
}

int main(void)
{
	uint8_t i;

	RCC -> AHBENR |= RCC_AHBENR_GPIOAEN | RCC_AHBENR_GPIOBEN;
	RCC -> APB2ENR |= RCC_APB2ENR_SYSCFGEN | RCC_APB2ENR_SPI1EN;

	GPIOA -> OSPEEDR = 0xFFFFFFFF;
	GPIOB -> OSPEEDR = 0xFFFFFFFF;
	GPIOA -> MODER |= GPIO_MODER_MODER1_0 | GPIO_MODER_MODER4_0;
	PWR_ON;
	WIFI_RESTART;
	delay_ms(100);
	WIFI_START;

	SYSCFG -> EXTICR[0] = 0;
	EXTI -> IMR |= EXTI_IMR_MR0;
	EXTI -> RTSR |= EXTI_RTSR_TR0;
	NVIC_SetPriority(EXTI0_1_IRQn, 1);
	NVIC_EnableIRQ(EXTI0_1_IRQn);

	delay_ms(1000);

	AT45DB161_Init();
//	Default();
	Flash_ReadSettings();

	DHT11_Init();
	ESP8266_Init();
	if (hotspot == '1'){
		i = ESP8266_ConnectToAP(sssid, spassw);
		if (i == OK){
			Copy(temp_buf, "AT+CIPSTA=\"", 11, 0);
			InsertStr(temp_buf, ip, 11);
			temp_buf[11 + Length(ip)] = '\"';
			temp_buf[11 + Length(ip) + 1] = 0;
			ESP8266_Write(temp_buf, 2, 1000, 2000);
		}
	}
	if ((hotspot == '0') || (i != OK)){
		ESP8266_CreateAP(mssid, mpassw);
	}
	ESP8266_CreateServer("80");

    while(1)
    {
    	if (CircleBuf_Get(&http)){
    		if (ID_Is_Connected(StrToUchar(http.ID))){
    			if (PARS_EqualStr(http.METHOD, "GET")){
    				if (PARS_EqualStr(http.URL, "/")){
    					stat = DHT11_Read(&Rh, &T);
    					if (!stat) {
    						BinToBCD(Rh, &num[3], &num[4], &num[0], &num[1], &num[2]);
    						if (num[0] == 48) num[0] = ' ';
    						num[3] = 0;
    						Copy(temp_buf, Main_Page, Length(Main_Page), 0);
    						FindStr(temp_buf, "��������� = ", &pos);
    						InsertStr(temp_buf, num, pos);
    						BinToBCD(T, &num[2], &num[3], &num[4], &num[0], &num[1]);
    						if (num[0] == 48) num[0] = ' ';
    						num[2] = 0;
    						FindStr(temp_buf, "����������� = ", &pos);
    						InsertStr(temp_buf, num, pos);
    					}
    					ESP8266_Send_HTTP_Head(http.ID, HTTP_MAIN, Length(temp_buf), 0);
    					ESP8266_Send_Content(http.ID, temp_buf, Length(temp_buf));
    				} else
   					if (PARS_EqualStr(http.URL, "/settings.html")){
   						if (http.parameter_counter > 0){
   							for (i = 0; i < http.parameter_counter; i++){
   								if (PARS_EqualStr(http.parameter_name[i], "hots")){
   									hotspot = http.parameter_value[i][0];
   								} else
   								if (PARS_EqualStr(http.parameter_name[i], "mssid")){
   									Copy(mssid, http.parameter_value[i], Length(http.parameter_value[i]), 0);
   								} else
   								if (PARS_EqualStr(http.parameter_name[i], "mpasw")){
   									Copy(mpassw, http.parameter_value[i], Length(http.parameter_value[i]), 0);
   								} else
   	   							if (PARS_EqualStr(http.parameter_name[i], "sssid")){
   	   								Copy(sssid, http.parameter_value[i], Length(http.parameter_value[i]), 0);
   	   							} else
   	   							if (PARS_EqualStr(http.parameter_name[i], "spasw")){
   	   								Copy(spassw, http.parameter_value[i], Length(http.parameter_value[i]), 0);
   	   							} else
   	   						   	if (PARS_EqualStr(http.parameter_name[i], "ip")){
   	   						   		Copy(ip, http.parameter_value[i], Length(http.parameter_value[i]), 0);
   	   						   	}
   							}
   							Flash_WriteSettings();
   						}
   						Flash_ReadSettings();
   						Copy(temp_buf, Settings_Page, Length(Settings_Page), 0);
   						if (hotspot == '0'){
   							FindStr(temp_buf, "value=\"0\" ", &pos);
   							InsertStr(temp_buf, "checked", pos);
   						} else {
   							FindStr(temp_buf, "value=\"1\" ", &pos);
   							InsertStr(temp_buf, "checked", pos);
   						}
   						FindStr(temp_buf, "name='mssid' size='20' value='", &pos);
   						InsertStr(temp_buf, mssid, pos);
   						FindStr(temp_buf, "name='mpasw' size='20' value='", &pos);
   						InsertStr(temp_buf, mpassw, pos);
   						FindStr(temp_buf, "name='sssid' size='20' value='", &pos);
   						InsertStr(temp_buf, sssid, pos);
   						FindStr(temp_buf, "name='spasw' size='20' value='", &pos);
   						InsertStr(temp_buf, spassw, pos);
   						FindStr(temp_buf, "name='ip' size='15' value='", &pos);
   						InsertStr(temp_buf, ip, pos);

   						ESP8266_Send_HTTP_Head(http.ID, HTTP_MAIN, Length(temp_buf), 0);
    					ESP8266_Send_Content(http.ID, temp_buf, Length(temp_buf));
    				} else {
    					ESP8266_Send_HTTP_Head(http.ID, HTTP_404, Length(Page_not_found), 0);
    					ESP8266_Send_Content(http.ID, Page_not_found, Length(Page_not_found));
    				}
    			}
    		}
    	}
    }
}
