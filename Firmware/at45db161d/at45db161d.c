#include "at45db161d.h"

uint8_t SPI_SendByte(SPI_TypeDef *spi, uint8_t byte){
	while(!(spi -> SR & SPI_SR_TXE));
	*(uint8_t *)((uint32_t)SPI1 + 0x0C) = byte;
	while(spi -> SR & SPI_SR_BSY);
	return *(uint8_t *)((uint32_t)SPI1 + 0x0C);
}

void AT45DB161_Init(){
	GPIOA -> MODER |= GPIO_MODER_MODER5_1 | GPIO_MODER_MODER6_1 | GPIO_MODER_MODER7_1;
	GPIOB -> MODER |= GPIO_MODER_MODER1_0;
	CS_HIGH;
	SPI1 -> CR2 = SPI_CR2_DS_0 | SPI_CR2_DS_1 | SPI_CR2_DS_2;
	SPI1 -> CR1 = SPI_CR1_SSM | SPI_CR1_SSI | SPI_CR1_MSTR | SPI_CR1_BR_2;
	SPI1 -> CR1 |= SPI_CR1_SPE;
}

void AT45DB161_Read_ID(uint8_t *MID, uint8_t *DID){
	CS_LOW;
	SPI_SendByte(SPI1, 0x9F);
	*MID = SPI_SendByte(SPI1, 0x00);
	*DID = SPI_SendByte(SPI1, 0x00);
	SPI_SendByte(SPI1, 0x00);
	SPI_SendByte(SPI1, 0x00);
	SPI_SendByte(SPI1, 0x00);
	SPI_SendByte(SPI1, 0x00);
	CS_HIGH;
}

uint8_t AT45DB161_Read_Status(){
	uint8_t temp;
	CS_LOW;
	SPI_SendByte(SPI1, 0xD7);
	temp = SPI_SendByte(SPI1, 0x00);
	CS_HIGH;
	return temp;
}

void AT45DB161_Read_Data(uint16_t page, uint16_t addr, uint32_t length, uint8_t *out){
	uint32_t i;
	while (!(AT45DB161_Read_Status() & 0x80));
	i = (page << 10) | addr;
	CS_LOW;
	SPI_SendByte(SPI1, 0x0B);
	SPI_SendByte(SPI1, i >> 16);
	SPI_SendByte(SPI1, i >> 8);
	SPI_SendByte(SPI1, i);
	SPI_SendByte(SPI1, 0x00);
	for (i = 0; i < length; i++){
		out[i] = SPI_SendByte(SPI1, 0xFF);
	}
	CS_HIGH;
}

void AT45DB161_PageProgram(uint16_t page, uint8_t *data, uint16_t length){
	uint16_t i;
	uint8_t temp;

	temp = AT45DB161_Read_Status();

	CS_LOW;
	SPI_SendByte(SPI1, 0x84);
	SPI_SendByte(SPI1, 0x00);
	SPI_SendByte(SPI1, 0x00);
	SPI_SendByte(SPI1, 0x00);
	for (i = 0; i < length; i++){
		SPI_SendByte(SPI1, data[i]);
	}
	CS_HIGH;
	CS_LOW;
	SPI_SendByte(SPI1, 0x83);
	if (temp & 0x01){	//512
		SPI_SendByte(SPI1, (uint8_t)(page >> 7));
		SPI_SendByte(SPI1, (uint8_t)((page & 0x7F) << 1));
		SPI_SendByte(SPI1, 0x00);
	} else {			//528
		SPI_SendByte(SPI1, (uint8_t)(page >> 6));
		SPI_SendByte(SPI1, (uint8_t)((page & 0x3F) << 2));
		SPI_SendByte(SPI1, 0x00);
	}
	CS_HIGH;
	while (!(AT45DB161_Read_Status() & 0x80));
}
